
import sys
from DRCParser import parser
from MySQL import *
from DRCNode import *
from random import *
import os
argdetail = []
finalfreeVariableMaxConj = []
finallimitedVariable = []
freeVariableMaxConjRecursion = []
limitedVariableRecursion = []
compVariable=[]
flag = 0
countNot = 0
count_exists =0
f=None
add_underscore=True

def read_input():
    s = ""
    while True:
        print('DRC> ', end='', flush=True)
        s1 = sys.stdin.readline().strip()
        if s1 == 'clear;':
            s= ""
        else:
            s += s1
            if s !='':
                if s[-1] == ";":
                    break
                s += ' '
    return s


# method to push the NOTs down in the treeNode
def pushNotsDown(treeNode):
    # Relation r = None
    if (treeNode is not None):
        if(treeNode.lchild == None and treeNode.rchild == None):
          pass
        elif (treeNode.rchild == None):
            # unary operator query, exists, forall, not
            if (treeNode.node_type == "not"):
                lc = treeNode.lchild
                lchildName = lc.node_type
                if (lchildName == "or"):
                    orLc = lc.lchild
                    orRc = lc.rchild
                    treeNode.node_type = "and"
                    if (orLc.node_type == "not"):
                        orLcChild = orLc.lchild
                        treeNode.lchild = orLcChild
                        pushNotsDown(orLcChild)
                    else:
                        lc = DRCNode()
                        treeNode.lchild = lc
                        lc.node_type = "not"
                        lc.lchild = orLc
                        pushNotsDown(lc)
                    if (orRc.node_type == "not"):
                        orRcChild = orRc.lchild
                        treeNode.rchild = orRcChild
                        pushNotsDown(orRcChild)
                    else:
                        rc = DRCNode()
                        treeNode.rchild = rc
                        rc.node_type("not")
                        rc.lchild = orRc
                        pushNotsDown(rc)
                
                else:
                    pushNotsDown(lc)

            else:  # query, exists, forall
                lc = treeNode.lchild
                pushNotsDown(lc)

        else:  # and , or
            lc = treeNode.lchild
            rc = treeNode.rchild
            pushNotsDown(lc)
            pushNotsDown(rc)
    return "OK"
# end of push NOTs down method

def semanticCheck(treeNode):
    if (treeNode !=None):
        if ((treeNode.lchild==None) and (treeNode.rchild==None)):
            if (treeNode.node_type=="comp"):
                freeVariable = []
                lopName =  treeNode.leftOperand[0]
                lopDataType =  treeNode.leftDataType[0]
                ropName =  treeNode.rightOperand[0]
                ropDataType =  treeNode.rightDataType[0]
                if(lopDataType.upper()=="STRING"):
                  ind = argdetail.index(lopName)
                  indcolumntype = ind + 2
                  lcolDataType = ""
                  if(ind >= 0):
                    lcolDataType =  argdetail[indcolumntype]
                  else:
                    return "Argument " + lopName + " not found"
                  if(ropDataType.upper()=="STRING"): # both operands are "col"
                    freeVariable.append(lopName)
                    freeVariable.append(ropName)
                    ind = argdetail.index(ropName.lower())
                    indcolumntype = ind + 2
                    rcolDataType = ""
                    if (ind >= 0):
                      rcolDataType =  argdetail[indcolumntype]
                    else:
                      return "Argument " + ropName + " not found"
                  else:
                    # lopDataType is "col" and ropDataType is "num" or "str"
                    freeVariable.append(lopName)
                    if ((lcolDataType=="VARCHAR" and ropDataType=="num") or
                        (lcolDataType=="DECIMAL" and ropDataType=="str") or
                        (lcolDataType=="INTEGER" and ropDataType=="str")):
                      return "Mismatch Types: " + lopName + " and " + ropName
                  treeNode.freeVarList=freeVariable
            else:
              freeVariable = []
              rname = treeNode.relationName.upper()
              if(rname in relations):
                atts = attributes[rname]
                arguments =treeNode.arguments
                if(len(atts) == len(arguments)/2):
                  doms = datatypes[rname]
                  j=0
                  for i in range(0,len(atts)):
                    domaintype =  doms[i]
                    attrname=  atts[i]
                    argname =  arguments[j]
                    datatype =  arguments[j+1]
                    j=j+2
                    if (datatype=="number" or datatype=="constant"):
                      if ((domaintype=="NUMBER" and datatype=="constant") or 
                          (domaintype=="STRING" and datatype=="number")):
                          return "Mismatch Types in ("+str(i+1)+") argument of relation: " + rname + " Required Type : "+domaintype+ " Available Type: "+datatype 
                    else:
                      freeVariable.append(argname)
                    argdetail.append(argname)
                    argdetail.append(attrname)
                    argdetail.append(domaintype)
                  treeNode.freeVarList=freeVariable
                  
                else:
                  return "Relation "+rname+" does not have same number of columns in query as in Database "
              else:
                return "Relation "+rname+" does not exist"

        elif(treeNode.rchild==None):           
           # unary operator query,not, exists, forall
          if(treeNode.node_type=="exists" or treeNode.node_type=="forall"):
            treeNode.count_exists_node=treeNode.count_exists_node+1
            freeVariableChild = []
            boundVariable = []
            lc = treeNode.lchild
            lc.count_exists_node=treeNode.count_exists_node
            boundVariable = treeNode.varlist
            for v in boundVariable:
              if boundVariable.count(v) > 1:
                return "multiple Bound variables with same name in FORALL or EXISTS (" + v + ")"
            treeNode.parent=treeNode
            lc.parent=treeNode
            semStatus = semanticCheck(lc)
            treeNode.count_exists_node=lc.count_exists_node
            if(semStatus.upper()=="OK"):
              freeVariableChild = lc.freeVarList
              freeVariableChildSet=set(freeVariableChild)
              boundVariableSet=set(boundVariable)
              x=list(freeVariableChildSet-boundVariableSet)
              treeNode.freeVarList=x
            else:
              return semStatus
          #end of if("exists,forall")
          elif(treeNode.node_type=="not"):
            freeVariable = []
            lc = treeNode.lchild
            lc.count_exists_node=treeNode.count_exists_node
            lc.parent=treeNode.parent
            semStatus = semanticCheck(lc)
            treeNode.count_exists_node=lc.count_exists_node
            if(semStatus.upper()=="OK"):
              freeVariable = lc.freeVarList
              treeNode.freeVarList=freeVariable
            else:
              return semStatus
              # end of if ("not")
          elif (treeNode.node_type=="query"):
            lc = treeNode.lchild
            lc.count_exists_node=treeNode.count_exists_node
            freeVarQuery = treeNode.varlist
            semStatus = semanticCheck(lc)
            treeNode.count_exists_node=lc.count_exists_node
            if(semStatus.upper()=="OK"):
              lc.parent=lc
              freeVar = lc.freeVarList   
              for x in freeVarQuery:
                if x in freeVar:
                  pass 
                else:
                  return "The free variables before '|' in Query ("+' '.join([str(elem)+',' for elem in freeVarQuery])[:-1]+") should be same as free variable after '|' in query ("+' '.join([str(elem)+',' for elem in freeVar])[:-1]+")"  
            else:
              return semStatus
          #end of if("query")
         #end of unary
        else:# and , or
          if(treeNode.node_type=="and"):
            freeVariable = []
            freeVariablelc = []
            freeVariablerc = []
            lc = treeNode.lchild
            rc = treeNode.rchild
            lc.count_exists_node=treeNode.count_exists_node
            lc.parent=treeNode.parent            
            semStatus1 = semanticCheck(lc)
            treeNode.count_exists_node=lc.count_exists_node
            rc.count_exists_node=treeNode.count_exists_node
            rc.parent=treeNode.parent
            semStatus2 = semanticCheck(rc)
            treeNode.count_exists_node=rc.count_exists_node
            if (semStatus1.upper()=="OK"):
                  if (semStatus2.upper()=="OK"):
                    freeVariablelc = lc.freeVarList
                    freeVariablerc = rc.freeVarList
                    freeVariable=freeVariablelc+freeVariablerc                    
                    treeNode.freeVarList=list(set(freeVariable))
                  else:
                    return semStatus2
            else:
              return semStatus1
    
          elif (treeNode.node_type=="or"):
            freeVariablelc = []
            freeVariablerc = []
            lc = treeNode.lchild
            rc = treeNode.rchild
            lc.count_exists_node=treeNode.count_exists_node
            lc.parent=treeNode.parent
            semStatus1 = semanticCheck(lc)
            treeNode.count_exists_node=lc.count_exists_node
            rc.count_exists_node=treeNode.count_exists_node
            rc.parent=treeNode.parent
            semStatus2 = semanticCheck(rc)
            treeNode.count_exists_node=rc.count_exists_node
            if(semStatus1.upper()=="OK"):
              if(semStatus2.upper()=="OK"):
                freeVariablelc =lc.freeVarList
                freeVariablerc =rc.freeVarList
                freeVariablelc.sort()
                freeVariablerc.sort()
                if freeVariablelc== freeVariablerc:
                  treeNode.freeVarList=freeVariablelc
                else:
                  return "Formulas connected with 'or' does not have same free variables : Left Formula Free variables are ("+' '.join([str(elem)+"," for elem in freeVariablelc])[:-1]+") and Right Formula Free Variables are ("+' '.join([str(elem)+',' for elem in freeVariablerc])[:-1]+")"
              else:
                return semStatus2
            else:
              return semStatus1
    # treeNode is not None
    return "OK"

 # end of semantic check method

def maxConjAndCheck(tree):
    global finalfreeVariableMaxConj,finallimitedVariable,freeVariableMaxConjRecursion,limitedVariableRecursion 
    if (tree != None):
      if ((tree.lchild==None) and (tree.rchild==None)):
        # comparision and predicate
        if(tree.node_type=="comp"):
          freeVariableMaxConj =  []
          limitedVariable =  []
          lopName =  tree.leftOperand[0]
          lopDataType =  tree.leftDataType[0]
          ropName =  tree.rightOperand[0]
          ropDataType =  tree.rightDataType[0]
          if (lopDataType=="col"):
            if (ropDataType=="col"):
              # both operands are "col"
              freeVariableMaxConj.append(lopName)
              freeVariableMaxConj.append(ropName)
              limitedVariable = None
              compVariable.append(lopName)
              compVariable.append(ropName)
            else:
              # lopDataType is "col" and ropDataType is "num" or "str"
              freeVariableMaxConj.append(lopName)
              limitedVariable.append(lopName)
              
            tree.freeVariableMaxConj=freeVariableMaxConj
            tree.limitedVarList=limitedVariable
        else: #predicate
          freeVariableMaxConj =  []
          arguments =tree.arguments
          temp =  []
          for i in range(0,len(arguments),2):
            #temp =arguments[i]
            argname =  arguments[i]
            datatype =  arguments[i+1]
            if(datatype=="col"):
              freeVariableMaxConj.append(argname)
          tree.freeVariableMaxConj=freeVariableMaxConj
          tree.limitedVarList=freeVariableMaxConj
      elif (tree.rchild==None):
        # unary operator query, exists, forall, not
        if (tree.node_type=="not"):
          limitedVariable =  []
          lc = tree.lchild
          maxConjAnd = maxConjAndCheck(lc)
          if(maxConjAnd.upper()=="OK"):
            if(lc.node_type=="predicate"):
              tree.freeVariableMaxConj=lc.freeVarList
              limitedVariable =None
              tree.limitedVarList=limitedVariable
            elif(lc.node_type=="exists"):
              boundVariable =  []
              boundVariable = lc.varlist
              lcChild = lc.lchild
              maxConjAndch= maxConjAndCheck(lcChild)
              if(maxConjAndch.upper()=="OK"):
                freeVar = lcChild.freeVarList
                ltdVar= lcChild.limitedVarList
                newFreeVar =  []
                freeVariableSize = len(freeVar)
                for i in range(0,freeVariableSize):
                  if freeVar[i] in boundVariable:
                    if(ltdVar is not None):
                      if freeVar[i] in ltdVar:
                        pass             
                      else:
                        ltdVar.append(freeVar[i])
                    else:
                      ltdVartemp =  []
                      ltdVartemp.append(freeVar[i])
                      ltdVar = ltdVartemp
                  else:
                    newFreeVar.append(freeVar[i])

                for j in range(0,len(newFreeVar)):
                  if(newFreeVar is not None):
                    if newFreeVar[j] in ltdVar:
                      ltdVar.remove(newFreeVar[j])
                    else:
                      pass
                  else:
                    pass
                tree.freeVariableMaxConj=newFreeVar
                tree.limitedVarList=ltdVar
              else:
                return maxConjAndch
            else:
              tree.freeVariableMaxConj=lc.freeVarList
              tree.limitedVarList=lc.limitedVarList
          else:
            return maxConjAnd
        elif(tree.node_type=="exists"): # exists
          boundVariable =  []
          limitedVariable =  []
          boundVariable = tree.varlist
          lc = tree.lchild
          maxConjAnd = maxConjAndCheck(lc)
          if(maxConjAnd.upper()=="OK"):
              boundVariable = tree.varlist
              freeVarChild = lc.freeVarList
              ltdVarChild = lc.limitedVarList
              newFreeVar =  []
              for i in range(0,len(freeVarChild)):
                if freeVarChild[i] in boundVariable:
                  if ltdVarChild is not None:
                    if freeVarChild[i] in ltdVarChild:
                      pass
                    else:
                      ltdVarChild.append(freeVarChild[i])
                  else:               
                    ltdVarChildtemp = []
                    ltdVarChildtemp.append(freeVarChild[i])
                    ltdVarChild = ltdVarChildtemp
                else:
                  pass

              tree.freeVariableMaxConj=freeVarChild
              tree.limitedVarList=ltdVarChild
          else:
            return maxConjAnd
        else: #query
          lc = tree.lchild
          maxConjAnd = maxConjAndCheck(lc)
          if(maxConjAnd.upper()=="OK"):
            tree.freeVariableMaxConj=lc.freeVarList
            tree.limitedVarList=lc.limitedVarList
          else:
            return maxConjAnd

      else: # and , or
          if(tree.node_type=="or"):
            lc = tree.lchild
            rc = tree.rchild
            freeVariableMaxConj =  []
            limitedVariable =  []
            maxConjAnd1 = maxConjAndCheck(lc)
            maxConjAnd2 = maxConjAndCheck(rc)
            if (maxConjAnd1=="OK"):
              if (maxConjAnd2=="OK"):
                pass
              else:
                return maxConjAnd2
            else:
              return maxConjAnd1
          elif (tree.node_type=="and"):
            freeVariableMaxConj =  []
            limitedVariable =  []
            # get the left and right child  of 'and' node
            lc = tree.lchild
            rc = tree.rchild
            # if the right child of 'and' node is also 'and' (conjuction of 'and')
            if(rc.node_type=="and"):
              # collect the free and limited variables of left child and store them in final free and limited var list
              maxConjAndCheck(lc)
              freeVariableMaxConj = lc.freeVarList
              limitedVariable = lc.limitedVarList
              freeVariableMaxConjSize =len(freeVariableMaxConj)
              for i in range(0,freeVariableMaxConjSize):
                if freeVariableMaxConj[i] in finalfreeVariableMaxConj:
                  pass
                else:
                  finalfreeVariableMaxConj.append(freeVariableMaxConj[i])

              if(limitedVariable == None):
                pass
              else:
                for i in range(0,len(limitedVariable)):
                  if limitedVariable[i] in finallimitedVariable:
                    pass
                  else:
                    finallimitedVariable.append(limitedVariable[i])
              
              # apply the same method maxConjAndCheck() to right child
              maxConjAnd = maxConjAndCheck(rc)
              if (maxConjAnd=="OK"):
                pass
              else:
                return maxConjAnd

            else: # case when the right child of 'and' node is not 'and' ( end of the maximal conjuction of 'and')
              freeVariableMaxConjlc=  []
              limitedVariablelc =  []
              freeVariableMaxConjrc=  []
              limitedVariablerc =  []
              # if the right child of 'and' is 'not', 'or' , 'exists' (possible case of recursive maximal conjuction of 'and')
              if(rc.node_type=="not" or rc.node_type=="or" or rc.node_type=="exists"):
                rcRcChild = rc.lchild
                maxConjAndRC = maxConjAndCheck(rcRcChild)
                if(maxConjAndRC.upper()=="OK"):
                  pass
                else:
                  return maxConjAndRC
                
                maxConjAnd1 = maxConjAndCheck(lc)
                if(maxConjAnd1.upper()=="OK"):
                  freeVariableMaxConjlc = lc.freeVarList
                  limitedVariablelc = lc.limitedVarList
                else:
                  return maxConjAnd1
        
                for i in range(0,len(freeVariableMaxConjlc)):
                  if freeVariableMaxConjlc[i] in finalfreeVariableMaxConj:
                    pass
                  else:
                    finalfreeVariableMaxConj.append(freeVariableMaxConjlc[i])

                if(limitedVariablelc == None):
                  pass
                else:
                  for i in range(0,len(limitedVariablelc)):
                    if limitedVariablelc[i] in finallimitedVariable:
                      pass
                    else:
                      finallimitedVariable.append(limitedVariablelc[i])
                   
                # store all the free variables and limited variables till this po
                for i in range(0,len(finalfreeVariableMaxConj)):
                  if finalfreeVariableMaxConj[i] in freeVariableMaxConjRecursion:
                    pass
                  else:
                    freeVariableMaxConjRecursion.append(finalfreeVariableMaxConj[i])

                if(finallimitedVariable == None):
                  pass
                else:
                  for i in range(0,len(finallimitedVariable)):
                      limitedVariableRecursion.append(finallimitedVariable[i])

                finalfreeVariableMaxConj =   []
                finallimitedVariable =   []

                #DRCNode rcRcChild = rc.lchild
                # maxConjAndRC = maxConjAndCheck(rcRcChild)

                #if (maxConjAndRC==("OK")){}
                #else{return maxConjAndRC}


                  #check if 'not' node or 'exists' node has child that is either predicate or comparision type
                if(rc.node_type=="not" or rc.node_type=="exists"):
                  lcNotOrExists = rc.lchild
                  if(lcNotOrExists.node_type=="comp" or lcNotOrExists.node_type=="predicate"):
                    maxConjAnd = maxConjAndCheck(lcNotOrExists)
                    freeVariableMaxConjrc = lcNotOrExists.freeVarList
                    if(rc.node_type=="not"):
                      limitedVariablerc = None
                    else:
                      limitedVariablerc = lcNotOrExists.limitedVarList
    
                  else:
                    maxConjAnd2 = maxConjAndCheck(rc)
                    if(maxConjAnd2.upper()=="OK"):
                      freeVariableMaxConjrc = rc.freeVarList
                      limitedVariablerc = rc.limitedVarList
                      for i in range(0,len(freeVariableMaxConjrc)):
                        if freeVariableMaxConjrc[i] in finalfreeVariableMaxConj:
                          pass
                        else:
                          finalfreeVariableMaxConj.append(freeVariableMaxConjrc[i])

                      if (limitedVariablerc == None):
                        pass
                      else:
                        for i in range(0,len(limitedVariablerc)):
                          if limitedVariablerc[i] in finallimitedVariable:
                            pass
                          else:
                            finallimitedVariable.append(limitedVariablerc[i])

                      for i in range(0,len(finalfreeVariableMaxConj)):
                          if finalfreeVariableMaxConj[i] in freeVariableMaxConjrc:
                            pass
                          else:
                            freeVariableMaxConjrc.append(finalfreeVariableMaxConj[i])  
                      if(limitedVariablerc == None):
                        pass
                      else:
                        for i in range(0,len(finallimitedVariable)):
                          if finallimitedVariable[i] in limitedVariablerc:
                            pass
                          else:
                            limitedVariablerc.append(finallimitedVariable[i])  
                              
                      for i in range(0,len(freeVariableMaxConjRecursion)):
                          if freeVariableMaxConjRecursion[i] in finalfreeVariableMaxConj:
                            pass
                          else:
                            finalfreeVariableMaxConj.append(freeVariableMaxConjRecursion[i]) 

                      if (limitedVariableRecursion == None):
                        pass
                      else:
                        for i in range(0,len(limitedVariableRecursion)):
                          if limitedVariableRecursion[i] in finallimitedVariable:
                            pass
                          else:
                            finallimitedVariable.append(limitedVariableRecursion[i]) 

                    else:
                      return maxConjAnd2
                          

                      # if rc node type is 'or'
                else:
                  maxConjAnd2 = maxConjAndCheck(rc)
                  if(maxConjAnd2.upper()=="OK"):
                    freeVariableMaxConjrc = rc.freeVarList
                    imitedVariablerc = rc.limitedVarList
                    for i in range(0,len(freeVariableMaxConjrc)):
                      if freeVariableMaxConjrc[i] in finalfreeVariableMaxConj:
                        pass
                      else:
                        finalfreeVariableMaxConj.append(freeVariableMaxConjrc[i])
                          
                    if (limitedVariablerc == None):
                      pass
                    else:
                      for i in range(0,len(limitedVariablerc)):
                        if limitedVariablerc[i] in finallimitedVariable:
                          pass
                        else:
                          finallimitedVariable.append(limitedVariablerc[i])

                      for i in range(0,len(finalfreeVariableMaxConj)):
                        if finalfreeVariableMaxConj[i] in freeVariableMaxConjrc:
                          pass
                        else:
                          freeVariableMaxConjrc.append(finalfreeVariableMaxConj[i])

                    if (limitedVariablerc == None):
                      pass
                    else:
                      for i in range(0,len(limitedVariablerc)):
                        if limitedVariablerc[i] in finallimitedVariable:
                          pass
                        else:
                          finallimitedVariable.append(limitedVariablerc[i])

                      for i in range(0,len(freeVariableMaxConjRecursion)):
                        if freeVariableMaxConjRecursion[i] in finalfreeVariableMaxConj:
                          pass
                        else:
                          finalfreeVariableMaxConj.append(freeVariableMaxConjRecursion[i])
                  

                      if(limitedVariableRecursion == None):
                        pass
                      else:
                        for i in range(0,len(limitedVariableRecursion)):
                          if limitedVariableRecursion[i] in finallimitedVariable:
                            pass
                          else:
                            finallimitedVariable.append(limitedVariableRecursion)


                      
                  else:
                    return maxConjAnd2
                      

              # if right child of 'and' is either 'predicate' or 'comparision' type ( end of the maximal conjuction of 'and' )
              elif (rc.node_type=="comp" or rc.node_type=="predicate"):
                maxConjAnd1 = maxConjAndCheck(lc)
                maxConjAnd2 = maxConjAndCheck(rc)

                if (maxConjAnd1=="OK"):
                  freeVariableMaxConjlc = lc.freeVarList
                  limitedVariablelc = lc.limitedVarList
                  if(maxConjAnd2=="OK"):
                    freeVariableMaxConjrc = rc.freeVarList
                    limitedVariablerc = rc.limitedVarList
                  else:
                    return maxConjAnd2
                else:
                  return maxConjAnd1

              for i in range(0,len(freeVariableMaxConjlc)):
                if freeVariableMaxConjlc[i] in finalfreeVariableMaxConj:
                  pass
                else:
                  finalfreeVariableMaxConj.append(freeVariableMaxConjlc[i])

              if (limitedVariablelc == None):
                pass
              else:
                for i in range(0,len(limitedVariablelc)):
                  if limitedVariablelc[i] in finallimitedVariable:
                    pass
                  else:
                    finallimitedVariable.append(limitedVariablelc[i])

              for i in range(0,len(freeVariableMaxConjrc)):
                if freeVariableMaxConjrc[i] in finalfreeVariableMaxConj:
                  pass
                else:
                  finalfreeVariableMaxConj.append(freeVariableMaxConjrc[i])             
              
              if(limitedVariablerc == None):
                pass
              else:
                for i in range(0,len(limitedVariablerc)):
                  if limitedVariablerc[i] in finallimitedVariable:
                    pass
                  else:
                    finallimitedVariable.append(limitedVariablerc[i])


                for i in range(0,len(compVariable),2):
                  if compVariable[i] in finallimitedVariable:
                    if compVariable[i+1] in finallimitedVariable:
                      pass
                    else:
                      finallimitedVariable.append(compVariable[i+1])
                  elif compVariable[i+1] in finallimitedVariable:
                    if limitedVariablerc[i] in finallimitedVariable:
                      pass
                    else:
                      finallimitedVariable.append(limitedVariablerc[i])


              #  to store the variable list that are found not limited
              notLimitedVar =  []
              for i in range(0,len(finalfreeVariableMaxConj)):
                if finalfreeVariableMaxConj[i] in finallimitedVariable:
                  pass
                else:
                  finallimitedVariable.append(finalfreeVariableMaxConj[i])

              if( len(notLimitedVar) !=0):
                return "The free variable " +notLimitedVar+" is not limited hence violating rule # 3 of Safe DRC formula"
            tree.limitedVarList=finallimitedVariable
    return "OK"
  # end of maximal conjuction of 'and' method

def checkNot(tree):
  global countNot,flag
  if(tree is not None):
    if ((tree.lchild==None) and (tree.rchild==None)):
      pass
    elif (tree.rchild==None):
      # unary operator query, exists, forall, not
      if (tree.node_type=="not"):
        return "rule # 4  of Safe DRC formula is violated : A 'not' operator can only be applied to a formula if it is connected to a non negated formula with an 'and'"
      else:
        #query, exists, forall
        lc = tree.lchild
        checkingNot = checkNot(lc)
        if(checkingNot.upper()=="OK"):
          pass
        else:
          return checkingNot
    else: #and , or
      lc = tree.lchild
      rc = tree.rchild
      if (tree.node_type=="or"):
        if(lc.node_type=="not" or rc.node_type=="not"):
          return "rule # 4 of Safe DRC formula is violated : A 'not' operator can only be applied to a formula if it is connected to a non negated formula with an 'and'"
        else:
          checkingNot1 = checkNot(lc)
          if(checkingNot1.upper()=="OK"):
            checkingNot2 = checkNot(rc)
            if(checkingNot2=="OK"):
              pass
            else:
              return checkingNot2
          else:
            return checkingNot1
      elif(tree.node_type=="and"):
        if(lc.node_type=="not"):
          countNot=countNot+1
        else:
          flag =1
        if(rc.node_type=="and"):
          checkingNot2 = checkNot(rc)
          if(checkingNot2.upper()=="OK"):
              pass
          else:
            return checkingNot2
        else:
          if(rc.node_type=="not"):
            if(flag==1):
              lcNot = rc.lchild
              flag =0
              countNot=0
              checkingNot2 = checkNot(lcNot)
              if (checkingNot2==("OK")):
                pass
              else:
                return checkingNot2
            else:
              return "rule # 4 of Safe DRC formula is violated : A 'not' operator can only be applied to a formula if it is connected to a non negated formula with an 'and'"

          else:
            flag=1
            if(countNot != 0 and flag==0):
              return "rule # 4 of Safe DRC formula is violated : A 'not' operator can only be applied to a formula if it is connected to a non negated formula with an 'and'"
            else:
              flag =0
              countNot=0
              checkingNot2 = checkNot(rc)
              if (checkingNot2.upper()=="OK"):
                pass
              else:
                return checkingNot2
                    
  return "OK"
    # end of safety rule 4 check method

def evaluateDRC(treeNode):
  global count_exists,f,add_underscore
  if(treeNode !=None):
    if (treeNode.lchild==None and treeNode.rchild==None):
      if(treeNode.node_type=="comp"):
        if(treeNode.leftDataType=="string"):
          treeNode.parent.predicate_varlist.append(treeNode.leftOperand)
          treeNode.predicate_varlist.append(treeNode.leftOperand)
        if(treeNode.rightDataType=="string"):
          treeNode.parent.predicate_varlist.append(treeNode.rightOperand)
          treeNode.predicate_varlist.append(treeNode.rightOperand)
        return treeNode.leftOperand.upper()+" "+treeNode.operator+" "+treeNode.rightOperand.upper()+","
      else:
        #predicate
        rname = treeNode.relationName.lower()
        arguments =treeNode.arguments
        final_variables=[]
        if(treeNode.parent is not None and treeNode.parent.predicate_varlist is not None):
          parent_Varlist=treeNode.parent.predicate_varlist   
          for arg in treeNode.arguments[::2]:
            if arg in treeNode.parent.varlist and arg not in parent_Varlist and add_underscore==True:
              final_variables.append("_")
            elif arg in treeNode.parent.varlist and arg in parent_Varlist:
              final_variables.append(arg)
            else:
              final_variables.append(arg)
        else:
          for arg in treeNode.arguments[::2]:
            if arg in treeNode.parent.varlist:
              final_variables.append("_")
            else:
              final_variables.append(arg)
        return rname+'('+''.join([str(arg).upper()+"," for arg in final_variables])[:-1]+').'
    elif(treeNode.rchild==None):
      #query,not, exists
      if(treeNode.node_type=="exists"):           
        lc = treeNode.lchild
        if(lc.node_type=="predicate"):
          freeVariableChild = [i for i in lc.arguments[::2]]
          freeVariableChildSet=set(freeVariableChild)
          boundVariableSet=set(treeNode.varlist)
          x=list(freeVariableChildSet-boundVariableSet)
          treeNode.freeVarList=x
          lc.parent.predicate_varlist=x
          evalStatus = evaluateDRC(lc)
        else:
          evalStatus = evaluateDRC(lc)
          freeVariableChild = lc.freeVarList
          freeVariableChildSet=set(freeVariableChild)
          boundVariableSet=set(treeNode.varlist)
          x=list(freeVariableChildSet-boundVariableSet)
          treeNode.freeVarList=x            
        if count_exists ==1:
          count_exists-=1
          return evalStatus
        elif count_exists >1:
          count_exists-=1
          print("temp"+str(count_exists+1)+"("+''.join([str(arg).upper()+"," for arg in treeNode.freeVarList])[:-1]+') :- \n'+evalStatus+"\n")
          f.write("temp"+str(count_exists+1)+"("+''.join([str(arg).upper()+"," for arg in treeNode.freeVarList])[:-1]+") :- \n"+evalStatus+"\n\n")
          return "temp"+str(count_exists+1)+"("+''.join([str(arg).upper()+"," for arg in treeNode.freeVarList])[:-1]+")."
      elif(treeNode.node_type=="not"):
          lc = treeNode.lchild
          if(lc.node_type=="predicate"):
            add_underscore=False
          evalStatus = evaluateDRC(lc)
          add_underscore=True
          treeNode.predicate_varlist=lc.freeVarList
          return "not "+evalStatus
      #end of if ("not")
      elif(treeNode.node_type=="query"):
          lc = treeNode.lchild
          evalStatus = evaluateDRC(lc)
          freeVar = lc.freeVarList
          f.write("answer("+' '.join([str(arg).upper()+"," for arg in treeNode.varlist])[:-1]+'):-'+"\n"+evalStatus)
          print("answer("+' '.join([str(arg).upper()+"," for arg in treeNode.varlist])[:-1]+'):-'+"\n"+evalStatus)
          return "answer("+' '.join([str(arg).upper()+"," for arg in treeNode.varlist])[:-1]+'):-'+"\n"+evalStatus        
        #end of if("query")
    else:# and , or
      if(treeNode.node_type=="and"):            
        lc = treeNode.lchild
        rc = treeNode.rchild
        if(lc.node_type=="predicate" and rc.node_type=="predicate"):
          for arg in lc.arguments[::2]:
            if arg in rc.arguments[::2]:
              rc.parent.predicate_varlist.append(arg)
              rc.predicate_varlist.append(arg)
            else:
              pass        
        evalStatus2 = evaluateDRC(rc)
        lc.parent.predicate_varlist=rc.predicate_varlist
        treeNode.predicate_varlist=rc.predicate_varlist
        evalStatus1 = evaluateDRC(lc)
        if(rc.node_type=="comp"):
          evalStatus2=evalStatus2[:-1]+'.'
        return evalStatus1[:-1]+",\n"+evalStatus2
      elif (treeNode.node_type=="or"):
          lc = treeNode.lchild
          rc = treeNode.rchild
          lc.parent=treeNode.parent
          evalStatus2 = evaluateDRC(rc)
          lc.parent.predicate_varlist=rc.predicate_varlist
          treeNode.predicate_varlist=rc.predicate_varlist
          evalStatus1 = evaluateDRC(lc)
          if(rc.node_type=="comp"):
            evalStatus2=evalStatus2[:-1]+'.'
          return evalStatus1[:-1]+","+evalStatus2
  # treeNode is not None    
# end of evaluate method
      

def main():
    global count_exists,f
    dbconnection(sys.argv[1],sys.argv[2])
    num=0
    if not os.path.exists('Queries'):
      os.makedirs('Queries')
    while True:
      s = read_input()
      if s[0:len(s)- 1].strip() == 'exit':
          break
      elif(s == 'SCHEMA;' or s == 'schema;'):
        displayDatabaseSchema()
      else:           
        if s !='':
          tree_node = parser.parse(s.strip())
          if tree_node is not None and type(tree_node) is not str:
            #Push down NOTs in the tree_node if NOT is sitting in front of 'or'
            push_nots = pushNotsDown(tree_node)
            semResult = semanticCheck(tree_node)
            if(semResult.upper()=="OK"):
              maxConjAnd = maxConjAndCheck(tree_node)
              if(maxConjAnd.upper()=="OK"):
                checkingNot = checkNot(tree_node)
                if(checkingNot.upper()=="OK"):
                  count_exists=tree_node.count_exists_node
                  filename="Queries/q"+str(num)
                  f= open(filename,"w+")
                  evalStatus=evaluateDRC(tree_node)
                  f.write("\n$")
                  f.close()
                  print("\n DRC query is converted to DLOG query and saved in "+filename+" file")
                  num+=1
                else:
                  print("\n SEMANTIC ERROR in DRC Query: "+checkingNot)
              else:
                print("\n SEMANTIC ERROR in DRC Query: "+maxConjAnd)
            else:
              print("\n SEMANTIC ERROR in DRC Query: "+semResult)
          
                
# program execution starts from here
main()