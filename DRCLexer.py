import ply.lex as lex

reserved = {
 'or':'OR',
  'and': 'AND',
  'exists':'EXISTS',
  'forall':'FORALL',
  'not':'NOT'
}

tokens = ['SEMICOLON','LPAREN','RPAREN','LBRACE','RBRACE','STRING','COMPARISON','COMMA','NAME','NUMBER','BAR'] + list(reserved.values())

t_SEMICOLON= r'\;'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_STRING=r'[\'][^\'\n]*[\']'
t_COMPARISON= r'<> |= | >= | <= | < | >' 
t_COMMA=r','
t_BAR=r'\|'

def t_NAME(t):
  r'[a-zA-Z][a-zA-Z0-9_]*'
  t.type = reserved.get(t.value.lower(),'NAME')
  return t

def t_NUMBER(t):
    r'[0-9]+ | [0-9]+"\."[0-9]* | "."[0-9]*'
    t.value = int(t.value)
    return t

# Ignored characters
t_ignore = " \r\n\t"
t_ignore_COMMENT = r'\#.*'

def t_error(t):
  print("Illegal character '%s'" % t.value[0])
  t.lexer.skip(1)

lexer = lex.lex()

