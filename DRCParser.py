import ply.yacc as yacc
from DRCLexer import tokens
from DRCNode import  DRCNode

def p_query(t):
    'query : LBRACE VarList BAR Formula RBRACE SEMICOLON'    
    node = DRCNode()
    node.node_type = "query"
    node.varlist = t[2]
    node.lchild=t[4]
    t[0] = node

def p_varList_name(t):
    'VarList : NAME'
    t[0] = [t[1]]


def p_varList_name2(t):
    'VarList : VarList COMMA NAME'
    t[0] = t[1]+[t[3]]

def p_Formula_AF(t):
    'Formula : AtomicFormula'
    t[0] = t[1]
  
def p_Formula_and(t):
    'Formula : Formula AND Formula'
    node = DRCNode()
    node.node_type='and'
    node.lchild=t[1]
    node.rchild=t[3]
    t[0] = node
  
def p_Formula_or(t):
    'Formula : Formula OR Formula'
    node = DRCNode()
    node.node_type='or'
    node.lchild=t[1]
    node.rchild=t[3]
    t[0] = node

def p_Formula_not(t):
    'Formula : NOT LPAREN Formula RPAREN'
    node = DRCNode()
    node.node_type='not'
    node.lchild=t[3]
    t[0] = node

def p_Formula_exits(t):
    'Formula : LPAREN EXISTS VarList RPAREN LPAREN Formula RPAREN'
    node = DRCNode()
    node.node_type='exists'
    node.varlist=t[3]
    node.lchild=t[6]
    t[0] = node
  
def p_Formula_forall(t):
    'Formula : LPAREN FORALL VarList RPAREN LPAREN Formula RPAREN'
    node = DRCNode()
    node.node_type='forall'
    node.varlist=t[3]
    node.lchild=t[6]
    t[0] = node
  

def p_AtomicFormula_name(t):
    'AtomicFormula : NAME LPAREN ArgList RPAREN'
    node = DRCNode()
    node.arguments=t[3]
    node.node_type="predicate"
    node.relationName=t[1]
    t[0] = node
  
def p_AtomicFormula_Arg(t):
    'AtomicFormula : Arg COMPARISON Arg'
    node = DRCNode()
    node.node_type="comp"
    node.leftOperand=t[1].split(".")[0]
    node.operator=t[2]
    node.leftDataType=t[1].split(".")[1]
    node.rightDataType=t[3].split(".")[1]
    node.rightOperand=t[3].split(".")[0]
    t[0] = node

def p_ArgList(t):
    'ArgList : Arg'
    t[0]=t[1].split(".")

def p_ArgList_2(t):
    'ArgList : ArgList COMMA Arg'
    t[0] = t[1]+t[3].split(".")    
def p_arg_name(t):
    'Arg : NAME'
    t[0] = t[1]+".string"

def p_arg_string(t):
    'Arg : STRING'
    t[0] = t[1]+".constant"

def p_arg_number(t):
    'Arg : NUMBER'
    t[0] = str(t[1])+".number"


def p_error(t):
    if t:
        print("Syntax error at '%s'" % t)
    else:
        print("Syntax error at EOF")

parser = yacc.yacc()
