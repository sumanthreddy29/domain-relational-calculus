class DRCNode:

    def __init__(self):
        self.lchild = None # left child
        self.rchild = None  # right child
        self.parent = None # used to store each parent rule
        self.predicate_varlist = [] # used to store predicate variables 
        self.node_type = None  # type of node exists,and , or , not , predicate
        self.count_exists_node=0 # to count no of exists in query so that we can generate that many Datalog rules
        self.arguments = []  # arguments in relation table
        self.relationName= None  #Table name
        self.varlist = [] #Variables in Query
        self.leftOperand= []  # used with comp
        self.leftDataType = []  # used with comp
        self.operator =[]  # used with comp
        self.rightOperand=[]     # used with comp
        self.rightDataType=[]     # num, str, col - used with comp 
        self.freeVarList=[]     # List of free variables at each node of the tree
        self.freeVariableMaxConj=[]  
        self.limitedVarList=[]     # List of limited variables at each node of the tree
        self.selVarList =[]    # List of variable list used for selection


    def sizeOfTree(self):
        if ((self.lchild==None) and (self.rchild==None)):
            return 1
        elif (self.rchild==None):
            return (1+self.lchild.sizeOfTree())
        else:
            return (1+self.lchild.sizeOfTree()+self.rchild.sizeOfTree())


    def printTree(self):
        if ((self.lchild == None) and ( self.rchild == None)):
            if (self.node_type=="comp"):
                print("::::Node:") 
                print("Node is: " + str(self.node_type) + "  ") 
                print(self.leftOperand,end=" ") 
                print(" , ") 
                print(self.leftDataType) 
                print("  ") 
                print(self.operator,end=" ") 
                print("  ") 
                print(self.rightOperand,end=" ") 
                print(" , ") 
                print( self.rightDataType) 
            else: #predicate node
                print("::::Node:") 
                print("Node is  : " + str(self.node_type)) 
                print("Relation name is  : " + self.relationName) 
                print("Arguments are : ( ",end=" ") 
                vec = self.arguments 
                #print(vec)
                temp = []
                if (vec!=None):
                    for i in range(0,len(vec)):
                        temp =vec[i]
                        print( temp+",",end=" ") 
                else:
                    print("None ")
            print(")") 
        elif ( self.rchild == None):  # query, exists, forall, not
            if (self.node_type=="exists" or self.node_type=="forall"):
                print("::::Node:") 
                print("Node Type: " + str(self.node_type) + "  ") 
                print("Variables are : ( ",end=" ") 
                vec = self.varlist 
                if (vec!=None):
                    for i in range(len(vec)):
                        print( vec[i]+",",end=" ")
                else:
                    print("None ) ")
                print(")") 
                self.lchild.printTree() 
            elif(self.node_type=="not"):
                print("::::Node:") 
                print("Node Type: " + str(self.node_type) + "  ") 
                self.lchild.printTree() 
            elif (self.node_type=="query"):
                print("::::Node:") 
                print("Node Type: " + self.node_type + "  ") 
                print("Select Variables : ( ", end =" ") 
                vec1 = self.varlist 
                if (vec1!=None):
                    for i in range(0,len(vec1)):
                        print( vec1[i]+",", end =" ") 
                else:
                    print("None ) ") 
                print(")") 
                #print(self.lchild.node_type)
                self.lchild.printTree() 
        else:     # and , or
            if (self.node_type=="and" or self.node_type=="or" ):
                print("::::Node:") 
                print("Node Type: "+self.node_type+"  ") 
                self.lchild.printTree() 
                self.rchild.printTree()
            
  
